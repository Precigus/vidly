﻿using System.Web.Mvc;

namespace Vidly.Controllers
{
    [AllowAnonymous]
    public class HomeController : Controller
    {
        [OutputCache(Duration = 50)]
        public ActionResult Index()
        {
            return View();
        }
    }
}