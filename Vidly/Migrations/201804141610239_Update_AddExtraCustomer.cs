using System.Web.UI.WebControls;

namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update_AddExtraCustomer : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO Customers (Name, IsSubscribedToNewsletter,MembershipTypeId, BirthDate) VALUES ('Andrew Perkins', 'True', 3, '1976-05-12')");
        }
        
        public override void Down()
        {
        }
    }
}
