namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update_BirthDateValues : DbMigration
    {
        public override void Up()
        {
            Sql("UPDATE Customers SET BirthDate = '1990-06-23' WHERE Id = 2");
        }
        
        public override void Down()
        {
        }
    }
}
