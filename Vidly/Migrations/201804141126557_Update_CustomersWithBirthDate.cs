namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update_CustomersWithBirthDate : DbMigration
    {
        public override void Up()
        {
            Sql("UPDATE Customers SET Name = 'Marry Williams' WHERE Id = 3");
            Sql("UPDATE Customers SET BirthDate = '1990-06-23' WHERE Id = 1");
        }
        
        public override void Down()
        {
        }
    }
}
