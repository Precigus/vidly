﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Services.Discovery;
using Newtonsoft.Json;

namespace Vidly.Models
{
    public class Movie
    {
        public int Id { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        public Genre Genre { get; set; }

        [Display(Name = "Genre")]
        public byte GenreId { get; set; }
        
        [Required]
        [Display(Name = "Release Date")]
        public DateTime ReleaseDate { get; set; }

        [Required]
        public DateTime DateAdded { get; set; }

        [Required]
        [Range(1, 20)]
        [Display(Name = "Number in Stock")]
        public byte InStock { get; set; }

        [Range(1, 20)]
        [Display(Name = "Available")]
        public byte NumberAvailable { get; set; }
    }
}